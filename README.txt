CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------
The Google Calendar Merge module uses Google OAuth2 Service Account to
authenticate a connection to the Google Calendar API as a service account. Once
the connection has been established, a cron task synchronizes all Google
calendars associated with the account with a local event calendar. 

Google calendar content instances are used to track synchronization with each
Google calendar accessible to the service account. Access to this content should
be restricted to users logged in with appropriate permissions.

Events are captured as GCal Event content. GCal Events include basic information
about the event--title (summary), date/time, and location. They also include the
event and calendar ID from the Google calendar.

Repeating events do not use the Date Repeat Entry. They do include the
"Repeating Parent ID" from the Google calendar.

The "GCal Events" view includes a tabbed event calendar showing monthly, weekly,
daily yearly, and all upcoming events. It also includes an "Upcoming Block" with
the next 5 events.

The Features module was used to define the "Google calendar" and "GCal Event"
content types. The machine name of all fields is either field_google_... or
field_gcal... This allows users to add additional fields without introducing 
conflicts with future releases.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/vgriffin/2390267

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2390267


REQUIREMENTS
------------
This module requires the following modules:
 * Calendar (https://www.drupal.org/project/calendar)
 * Chaos tools (https://www.drupal.org/project/ctools)
 * Date/Date API/Date Views (https://www.drupal.org/project/date)
 * Entity API (https://www.drupal.org/project/entity)
 * Field Permissions (https://www.drupal.org/project/field_permissions)
 * Google OAuth2 Service Account
   (https://www.drupal.org/sandbox/vgriffin/2390251)
 * Libraries (https://www.drupal.org/project/libraries)
 * Simple Google Maps (https://www.drupal.org/project/simple_gmap)
 * Text (Core Drupal)
 * Variable (https://www.drupal.org/project/variable)
 * Views (https://www.drupal.org/project/views)

It was developed using Features and requires that module for updates

RECOMMENDED MODULES
-------------------
 * Rules (https://www.drupal.org/project/rules)
   Rules may be used to set additional fields when GCal Event nodes are created.

 * Elysia Cron (https://www.drupal.org/project/elysia_cron)
   Ensure that updates are applied in a timely fashion.


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * There are several configurations which cannot be captured and exported by
   Features. Therefore, additional configuration is required after this module
   is installed.

   - The displays in the "GCal Events" view use shorter fields, and time-only
     fields. These date display formats must be added for the views to work. The
     Google calendar processing times include seconds in the display. 
     
     - Add the following formats, or their regional equivalents, to the list
       shown in admin/config/regional/date-time/formats

	     D, M j, Y, g:ia
       D, M j, Y, g:i:sa
	     g:ia

     - Under TYPES, associate these with "Short display", "Display seconds", and
       "Time only, ap" respectively. Associate "Shortest display" with the
       format "mm/dd/yyyy - hh:mmap" or its regional equivalent.
       
 * (Optional) Add additional fields to the content types "GCal Event" and
   "Google calendar" as desired. It is expected that more fields will be added
   to "GCal Event" as that content type is website-facing.
   
   These fields may be used for site-specific functions such as flags to control
   display or links to event registration processing. The machine name for these
   fields must not start with "field_google" or "field_gcal".

 * (Optional) Clone the "GCal Events" view and edit the copy to display new
   fields and use conditions as needed.

 * The Google-specific fields are hidden from everyone except the author and 
   administrators.

 * (Optional) Set additional permissions. It is recommended that Google calendar
   nodes be hidden from everyone except administrators. It is also recommended
   that GCal Event creation be permitted for administrators only. (The cron task
   uses account 1 for all operations.) Changes to standard fields will be passed
   back to the Google calendar event if the original event has not changed since
   the most recent update. It is not possible to create new events on the
   website and push them to the Google calendar.

 * Configure the content types "GCal Event" and "Google calendar" with the
   following options.

	   Publish, but do not promote to Front Page
	   Close comments
	   Do not post author information

 * (Optional) Define Rules for import processing. These Rules may be used to set
   values of the additional fields added to content types above.

   If your Rules are based on node creation, changes made to local fields after
   node creation will not be affected for subsequent display.

   Use cases include a "show-in-upcoming" flag that controls event display in
   the Upcoming Block based on which Google calendar contains the event, whether
   or not it's part of a repeating series, and key words in the event title.

   If you use a specific Repeating Parent ID in a rule, use the "Starts with" 
   condition and do not include the separating underscore (_) character in the
   test. The Repeating Parent ID is visible in the GCal Event content or by
   using developer functions available through Google Apps.

 * (Optional) The defaut cron processing imports calendar updates only once a
   day. This is not generally frequent enough. Updates can occur as often as
   once every 5 minutes, although every 10 minutes is probably often enough.
   Configure the cron task to use the desired update rate.
   
 * (Optional) The default number of weeks into the future for repeating event
   creation is 60. This ensures that yearly events created to occur within the
   next 6 weeks or so will include two events in the website, even if the event
   is based on the day of the week. If a different lookahead time is desired,
   set the variable "Max repeat weeks" to the desired value.

 * (Optional) The process that determines what, if any, future repeating events
   to create is resource-intensive enough that it should not be run as often as
   the import process, especially on a shared host. The process of extending
   existing repeating events may be run only once a day, or even less
   frequently. By default, it will be run once a day. If a different frequency
   is desired, set the variable "Min repeat process hours" to the desired value.
   
 * (Optional) To force repeating events to occur at a particular time, set the 
   "Google calendar" field "Repeat proc start" to "Min repeat process hours"
   before the desired run time. Note that the time may be different for
   different calendars to minimize the system hit.

 * To completely rebuild the local calendar, remove all GCal events, clear the 
   "Sync token" and "Merge start" fields in all Google calendar content, and run
   the Google Calendar Merge cron task.
