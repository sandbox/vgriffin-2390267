<?php
/**
 * @file
 * google_calendar_merge.features.inc
 */

/**
 * Implements hook_views_api().
 */
function google_calendar_merge_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function google_calendar_merge_node_info() {
  $items = array(
    'gcal_event' => array(
      'name' => t('GCal Event'),
      'base' => 'node_content',
      'description' => t('Event synchronized with Google Calendar'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'google_calendar' => array(
      'name' => t('Google calendar'),
      'base' => 'node_content',
      'description' => t('Define Google calendar for event synchronization. The records and featured fields are maintained by the calendar merge (synchronization) process.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
